#!/bin/bash
#

src=https://raw.githubusercontent.com/jpetazzo/container.training/main/slides


remotefiles=( \
	docker-compose.yaml \
	Dockerfile \
	requirements.txt \
	build.sh \
	index.py \
	markmaker.py \
	workshop.html \
	workshop.css \
	index.css \
	remark.min.js \
)
# index.html
# index.yaml
# interstitials.txt
# readmes
# readme.yml


[ -d slides ] || mkdir slides  || {
	printf -- "Error\n"
        exit 1
}

printf -- "ok\n"


pushd slides >/dev/null || {
	printf -- "Can't go to slides dir ... strange ...\n"
        exit 1
}


printf -- "Downloading files from %s\n" "$src"
for f in "${remotefiles[@]}"
do
	[ -f "$f" ] || {
		printf -- " - %s " "$f"
		curl --silent "$src"/"$f" -o "$f" || {
			printf -- "error\n"
		        exit 1
		}
		printf -- "ok\n"
	}
done

[ -d .git ] || [ -d ../.git ] || {
	git init -q . || {
	    printf -- "Can't git init\n"
            exit 1
        }
}

chmod +x *.sh *.py

git add "${remotefiles[@]}" && git commit -q -m'First download from jpetazzo'

printf -- "Configure first env\n"

[ -f interstitials.txt ] || echo "no-url-defined" > interstitials.txt

# To run docker with current user writes
[ -f .env ] || ( echo "UID=$(id -u)"; echo "GID=$(id -g)" ) > .env

sed -i \
	-e 's/- 80/- 8888:80/' \
	-e 's/^\([ ]*\)builder:/\1builder:\n\1  user: "${UID}:${GID}"/' \
        docker-compose.yaml


[ -f doc/slides-howto.md ] || {

    mkdir doc || {
    	printf -- "Error creating doc dir...\n"
            exit 1
    }
    
    cat > doc/slides-howto.md <<EOF_FIRST
# Sample doc

- To add slides in this doc subject

  - edit doc/slides-howto.md
  - OR create a new md file and add the path in doc.yml content list
    
EOF_FIRST
}


[ -f doc.yml ] || cat > doc.yml <<EOF_DOC
---
title: |
  Howto make slides like jpetazzo

# Url to the chat room
#chat: 

# url to the git repository
gitrepo: url/to/this/repo.git

# Url to online slides
slides: http://localhost:8000/

#slidenumberprefix: "#SomeHashTag &mdash; "

# Exclude files or dir
exclude:
- assets

# List of md files
content:
  - doc/slides-howto.md

EOF_DOC



[ -f index.yaml ] || { 
	today="$(date +%Y-%m-%d)"
	i="$(date +%s)"
	j="$(( i + ( 24 * 3600 ) ))"
	tomorrow="$(date -d "@$j" +%Y-%m-%d)"


	cat > index.yaml<<EOF_INDEX
- date: [$today, $tomorrow]
  country: www
  city: my computer
  event: Personal
  speaker: $(id -un) 
  title: Develop slides from jpettazo
  lang: $LANG
  #attend: none
  slides: doc.yml.html

EOF_INDEX

}



cp ../patches/index.py .

git add .env index.py docker-compose.yaml index.yaml interstitials.txt doc.yml doc/slides-howto.md \
	&& git commit -m 'First files'

cat <<EOF_TODO

    cd slides
    docker compose up -d
    xdg-open http://localhost:8888/

EOF_TODO


# mypersonnaldoc

## Description

There are 2 things in this project

* the tools to manage slides and export documentation
* the doc itself

The slides tools are based on [tools](https://github.com/jpetazzo/container.training/tree/main/slides)
maintained by Jérôme Petazzoni/[@jpetazzo](https://twitter.com/jpetazzo)
for [container.training](https://container.training)

The technical tools are:

* [markdown](https://daringfireball.net/projects/markdown/) to write documents
* [remark](https://github.com/remarkjs) for md to html slideshow
* [python](https://python.org) and [yaml](https://yaml.org/) for internal tooling
* [docker](https://docs.docker.com/) to build html slides.


## Installation

For the first installation:

Create a local repository and get the init scripts

    mkdir -p mydoc/patches
    cd mydoc
    curl -OL https://gitlab.com/fccagou/mypersonnaldoc/-/raw/main/init.sh
    curl -L https://gitlab.com/fccagou/mypersonnaldoc/-/raw/main/patches/index.py \
         -o patches/index.py

Modify values at the top of `patches/index.py` to match your context

    title="My personnal docs"
    maintainer="François Chenais"
    twitterlogin="fccagou"
    contributorsurl="https://gitlab.com/fccagou/mypersonnaldoc/-/project_members"

Run `init.sh` script:

    bash ./init.sh


## Usage

### Updating slides

Run docker container used to generate doc:

    cd slides
    docker compose up -d
    xdg-open http://localhost:8888/

To add entry in `index.html`, add entry in `slides/index.yaml`:

    # New entry
    - date: [2022-12-04, 2022-12-05]          # Doc creation date
      country: www                            # Country where presentation is done
      city: my computer                       # City of the présentation
      event: Personal                         # The name of the event
      speaker: fccagou                        # The speaker
      title: Develop slides from jpettazo     # Workshop title
      lang: fr                                # Langage
      #attend: none                           # The attend url
      slides: doc.yml.html                    # The slides url

To add a new workshop, create a new yaml manifest `slides/workshopxxx.yml`:

    ---
    title: |
      The title of your workshop
     
    # Url to the chat room
    #chat:
     
    # url to the git repository
    gitrepo: url/to/this/repo.git
     
    # Url to online slides
    slides: http://localhost:8000/
     
    #slidenumberprefix: "#SomeHashTag &mdash; "
     
    # Exclude files or dir
    exclude:
    - assets
     
    # List of md files used for this worshop
    content:
      - workshopxxxx/introduction.md
      - workshopxxxx/part01.md
      - workshopxxxx/part02.md

And create all the new md files with the content of your workshop.

Using `build.sh`, the builder's container detects slides changes and
rebuild all the doc and `slides.zip` containig all html slides.

## Support

You can ask support or make merge request through the gitlab interface.

## Roadmap

Because it's a new side project, there is actually no roadmap.

Studying others tools like

* [pandoc](https://pandoc.org) the doc swiss knife.
* [sphinx](https://www.sphinx-doc.org/) python doc generator.
* [pelican](https://github.com/getpelican/) or [hugo](https://gohugo.io/) for static doc


## Contributing

This project aim is to centralize technical documentation. Averybody can contribute.

## Authors and acknowledgment

Author: fccagou



## License

The code in this repository is licensed under the Apache License
Version 2.0. See [license file](LICENSE)

## Project status

This is a new project with many changes.

